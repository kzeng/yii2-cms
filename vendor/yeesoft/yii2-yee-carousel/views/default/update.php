<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model yeesoft\carousel\models\Carousel */

$this->title = Yii::t('yee', 'Update "{item}"', ['item' => $model->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('yee/carousel', 'Carousels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('yee', 'Update');
?>

<div class="carousel-update">
    <h3 class="lte-hide-title"><?= Html::encode($this->title) ?></h3>
    <?= $this->render('_form', compact('model')) ?>
</div>


