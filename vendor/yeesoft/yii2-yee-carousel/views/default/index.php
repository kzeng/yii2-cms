<?php

use yeesoft\grid\GridPageSize;
use yeesoft\grid\GridQuickLinks;
use yeesoft\grid\GridView;
use yeesoft\helpers\Html;
use yeesoft\models\User;
use yeesoft\carousel\models\Carousel;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel yeesoft\carousel\models\search\CarouselSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('yee/carousel', 'Carousels');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carousel-index">

    <div class="row">
        <div class="col-sm-6">
            <h3 class="lte-hide-title page-title"><?= Html::encode($this->title) ?></h3>
            <?= Html::a(Yii::t('yee', 'Add New'), ['/carousel/default/create'], ['class' => 'btn btn-sm btn-primary']) ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-12 text-right">
                    <?= GridPageSize::widget(['pjaxId' => 'page-grid-pjax']) ?>
                </div>
            </div>

            <?php Pjax::begin(['id' => 'carousel-grid-pjax']) ?>

            <?=
            GridView::widget([
                'id' => 'carousel-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'bulkActionOptions' => [
                    'gridId' => 'carousel-grid',
                    'actions' => [
                        Url::to(['bulk-delete']) => Yii::t('yii', 'Delete'),
                    ]
                ],
                'columns' => [
                    ['class' => 'yeesoft\grid\CheckboxColumn', 'options' => ['style' => 'width:10px']],
                    [
                        'attribute' => 'img_url',
                        'class' => 'yeesoft\grid\columns\TitleActionColumn',
                        'controller' => '/carousel/default',
                        'title' => function (Carousel $model) {
                            return Html::a('<img src='.$model->img_url.' width=120 height=60>', ['/carousel/default/update', 'id' => $model->id], ['data-pjax' => 0]);
                        },
                        'buttonsTemplate' => '{update} {delete}',
                    ],

                    [
                        'attribute' => 'title',
                        //'class' => 'yeesoft\grid\columns\TitleActionColumn',
                        //'options' => ['style' => 'width:30px'],
                    ],
        
                    [
                    		'attribute' => 'link',
                    		//'class' => 'yeesoft\grid\columns\TitleActionColumn',
                    		//'options' => ['style' => 'width:30px'],
                    ],
                    
                    [
                    		'attribute' => 'sort',
                    		//'class' => 'yeesoft\grid\columns\TitleActionColumn',
                    		//'options' => ['style' => 'width:30px'],
                    ],
                    
                    [
                        'attribute' => 'status',
                    	'class' => 'yeesoft\grid\columns\TitleActionColumn',
                        //'class' => 'yeesoft\grid\columns\StatusColumn',
                        //'options' => ['style' => 'width:30px'],
                    	'title' => function (Carousel $model) {
                    		if($model->status == 0)
                    			return '无效';
                    		else
                    			return '有效';
                        },
                        'buttonsTemplate' => '{update} {delete}',
                    ],

       

                ],
            ]);
            ?>

            <?php Pjax::end() ?>
        </div>
    </div>
</div>


