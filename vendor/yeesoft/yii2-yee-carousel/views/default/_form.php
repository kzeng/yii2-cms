<?php

use yeesoft\helpers\Html;
use yeesoft\models\User;
use yeesoft\widgets\ActiveForm;
use yeesoft\widgets\LanguagePills;

use yeesoft\carousel\models\Carousel;
//use dosamigos\fileupload\FileUpload;
use dosamigos\fileupload\FileUploadUI;

/* @var $this yii\web\View */
/* @var $model yeesoft\carousel\models\Carousel */
/* @var $form yeesoft\widgets\ActiveForm; */
?>

<div class="carousel-form">

    <?php
    $form = ActiveForm::begin([
                'id' => 'carousel-form',
                'validateOnBlur' => false,
            ])
    ?>

    <div class="row">
        <div class="col-md-9">

            <div class="panel panel-default">
                <div class="panel-body">

                    <?php if ($model->isMultilingual()): ?>
                        <?= LanguagePills::widget() ?>
                    <?php endif; ?>

                    <!--
					<//?= FileUploadUI::widget([
					    'model' => $model,
					    'attribute' => 'image',
					    'url' => ['/carousel/default/imageupload'],
					    'gallery' => false,
					    'fieldOptions' => [
					        'accept' => 'image/*'
					    ],
					    'clientOptions' => [
					        'maxFileSize' => 2000000
					    ],
					    // ...
					    'clientEvents' => [
					        'fileuploaddone' => 'function(e, data) {
													
					                                //console.log(e);
					                                //console.log(data);

													console.log(data.result.files[0].url);
													$("#carousel-img_url").val(data.result.files[0].url);

													console.log("fileuploaddone");
					                            }',
					        'fileuploadfail' => 'function(e, data) {
					                                //console.log(e);
					                                console.log(data);
													console.log("fileuploadfail");
					                            }',
					    ],
					]); ?>
                    -->

                    <?= $form->field($model, 'image')->widget(yeesoft\media\widgets\FileInput::className(), [
                        'name' => 'image',
                        'buttonTag' => 'button',
                        'buttonName' => Yii::t('yee', 'Browse'),
                        'buttonOptions' => ['class' => 'btn btn-default btn-file-input'],
                        'options' => ['class' => 'form-control'],
                        'template' => '<div class="post-thumbnail thumbnail"></div><div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                        //'thumb' => $this->context->module->thumbnailSize,
                        'imageContainer' => '.post-thumbnail',
                        'pasteData' => yeesoft\media\widgets\FileInput::DATA_URL,
                        'callbackBeforeInsert' => 'function(e, data) {
                                console.log("callbackBeforeInsert ...");
                                console.log(data["url"]);
                                $("#carousel-img_url").val(data["url"]);
                                $(".post-thumbnail").show();
                            }',
                    ]) ?>

					<?php if(!empty($model->img_url)) {?>
						<img src="<?php echo $model->img_url ?>" width=240 height=120>
					<?php } ?>
	
                    <?= $form->field($model, 'img_url')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>
                    
                    <?= $form->field($model, 'status')->dropDownList(Carousel::getStatusOption()) ?>

                </div>
            </div>
        </div>

        <div class="col-md-3">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="record-info">

                        <div class="form-group">
                            <?php if ($model->isNewRecord): ?>
                                <?= Html::submitButton(Yii::t('yee', 'Create'), ['class' => 'btn btn-primary']) ?>
                                <?= Html::a(Yii::t('yee', 'Cancel'), ['/carousel/default/index'], ['class' => 'btn btn-default',]) ?>
                            <?php else: ?>
                                <?= Html::submitButton(Yii::t('yee', 'Save'), ['class' => 'btn btn-primary']) ?>
                                <?=
                                Html::a(Yii::t('yee', 'Delete'), ['/carousel/default/delete', 'id' => $model->id], [
                                    'class' => 'btn btn-default',
                                    'data' => [
                                        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ])
                                ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
