<?php

namespace yeesoft\carousel\models;

use yeesoft\behaviors\MultilingualBehavior;
use yeesoft\models\OwnerAccess;
use yeesoft\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yeesoft\db\ActiveRecord;

/**
 * This is the model class for table "{{%carousel}}".
 *
 * @property integer $id
 * @property string $slug
 * @property string $content
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class Carousel extends ActiveRecord 
{
	
	const STATUS_INACTIVE= 0;
	const STATUS_ACTIVE= 1;
	
	public $image;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%carousel}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            //BlameableBehavior::className(),
            // 'multilingual' => [
            //     'class' => MultilingualBehavior::className(),
            //     'langForeignKey' => 'carousel_id',
            //     'tableName' => "{{%carousel_lang}}",
            //     'attributes' => [
            //         'content',
            //     ]
            // ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','img_url','title','link'], 'string'],
            [['sort', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */

    //id  title   img_url link    sort    status     created_at  updated_at

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yee', 'ID'),
            'title' => Yii::t('yee', 'Title'),
            'img_url' => Yii::t('yee/carousel', 'Image URL'),
            'link' => Yii::t('yee', 'Link'),
            'sort' => Yii::t('yee', 'Sort'),
            'status' => Yii::t('yee', 'Status'),
            'created_at' => Yii::t('yee', 'Created'),
            'updated_at' => Yii::t('yee', 'Updated'),
        ];
    }

    /**
     * @inheritdoc
     * @return CarouselQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarouselQuery(get_called_class());
    }

  
    /**
     * @return \yii\db\ActiveQuery
     */

    public function getCreatedDate()
    {
        return Yii::$app->formatter->asDate(($this->isNewRecord) ? time() : $this->created_at);
    }

    public function getUpdatedDate()
    {
        return Yii::$app->formatter->asDate(($this->isNewRecord) ? time() : $this->updated_at);
    }

    public function getCreatedTime()
    {
        return Yii::$app->formatter->asTime(($this->isNewRecord) ? time() : $this->created_at);
    }

    public function getUpdatedTime()
    {
        return Yii::$app->formatter->asTime(($this->isNewRecord) ? time() : $this->updated_at);
    }

    public function getCreatedDatetime()
    {
        return "{$this->createdDate} {$this->createdTime}";
    }

    public function getUpdatedDatetime()
    {
        return "{$this->updatedDate} {$this->updatedTime}";
    }

    /**
     *
     * @inheritdoc
     */
    public static function getFullAccessPermission()
    {
        return 'fullCarouselAccess';
    }
    

    static function getStatusOption($key=null)
    {
    	$arr = array(
    			self::STATUS_INACTIVE=> '无效',
    			self::STATUS_ACTIVE=> '有效',
    	);
    	return $key === null ? $arr : (isset($arr[$key]) ? $arr[$key] : '');
    }
    

}