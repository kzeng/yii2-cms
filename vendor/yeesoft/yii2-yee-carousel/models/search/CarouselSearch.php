<?php

namespace yeesoft\carousel\models\search;

use yeesoft\carousel\models\Carousel;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CarouselSearch represents the model behind the search form about `yeesoft\carousel\models\Carousel`.
 */
class CarouselSearch extends Carousel
{

    /**
     * @inheritdoc
     */

    //id  title   img_url link    sort    status     created_at  updated_at

    public function rules()
    {
        return [
            [['id', 'sort', 'status', 'created_at', 'updated_at'], 'integer'],
            [['title', 'img_url', 'link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //$query = Carousel::find()->joinWith('translations');
        $query = Carousel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Yii::$app->request->cookies->getValue('_grid_page_size', 20),
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        
        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }

}
