<?php

use yii\db\Migration;

class m170525_131719_add_carousel_menu_links extends Migration
{
    // public function up()
    // {

    // }

    // public function down()
    // {
    //     echo "m170525_131719_add_carousel_menu_links cannot be reverted.\n";

    //     return false;
    // }


    public function up()
    {
        $this->insert('{{%menu_link}}', ['id' => 'carousel', 'menu_id' => 'admin-menu', 'link' => '/carousel/default/index', 'image' => 'film', 'created_by' => 1, 'order' => 18]);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'carousel', 'label' => 'Carousel', 'language' => 'en-US']);
    }

    public function down()
    {
        $this->delete('{{%menu_link_lang}}', ['like', 'link_id', 'carousel']);
        $this->delete('{{%menu_link}}', ['like', 'id', 'carousel']);

    }


    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
