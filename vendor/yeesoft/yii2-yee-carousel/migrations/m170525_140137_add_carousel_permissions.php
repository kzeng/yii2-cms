<?php

use yeesoft\db\PermissionsMigration;

class m170525_140137_add_carousel_permissions extends PermissionsMigration
{
  public function beforeUp()
    {
        $this->addPermissionsGroup('carouselManagement', 'Carousel Management');
    }

    public function afterDown()
    {
        $this->deletePermissionsGroup('carouselManagement');
    }

    public function getPermissions()
    {
        return [
            'carouselManagement' => [
                'links' => [
                    '/admin/carousel/*',
                    '/admin/carousel/default/*',
                ],
                'viewCarousels' => [
                    'title' => 'View Carousels',
                    'roles' => [self::ROLE_MODERATOR],
                    'links' => [
                        '/admin/carousel/default/index',
                        '/admin/carousel/default/grid-sort',
                        '/admin/carousel/default/grid-page-size',
                    ],
                ],
                'editCarousels' => [
                    'title' => 'Edit Carousels',
                    'roles' => [self::ROLE_MODERATOR],
                    'childs' => ['viewCarousels'],
                    'links' => [
                        '/admin/carousel/default/update',
                    ],
                ],
                'createCarousels' => [
                    'title' => 'Create Carousels',
                    'roles' => [self::ROLE_MODERATOR],
                    'childs' => ['viewCarousels'],
                    'links' => [
                        '/admin/carousel/default/create',
                    ],
                ],
                'deleteCarousels' => [
                    'title' => 'Delete Carousels',
                    'roles' => [self::ROLE_MODERATOR],
                    'childs' => ['viewCarousels'],
                    'links' => [
                        '/admin/carousel/default/delete',
                        '/admin/carousel/default/bulk-delete',
                    ],
                ],
                'fullCarouselAccess' => [
                    'title' => 'Full Carousel Access',
                    'roles' => [self::ROLE_MODERATOR],
                ],
            ],
        ];
    }
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
