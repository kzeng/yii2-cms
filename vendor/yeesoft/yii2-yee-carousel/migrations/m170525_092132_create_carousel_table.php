<?php

use yii\db\Migration;

class m170525_092132_create_carousel_table extends Migration
{
    // public function up()
    // {

    // }

    // public function down()
    // {
    //     echo "m170525_092132_create_carousel_table cannot be reverted.\n";

    //     return false;
    // }

    const CAROUSEL_TABLE = '{{%carousel}}';
    
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable(self::CAROUSEL_TABLE, [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'img_url' => $this->string(512)->notNull(),
            'link' => $this->string(512)->notNull(),
            'sort' => $this->integer(1)->notNull()->defaultValue(255),
            'status' => $this->integer(1)->notNull()->defaultValue(0)->comment('0-pending,1-published'),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable(self::CAROUSEL_TABLE);
    }


}
