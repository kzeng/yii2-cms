<?php
/**
 * @link http://www.yee-soft.com/
 * @copyright Copyright (c) 2015 Zengkai
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace yeesoft\carousel;

use Yii;

/**
 * Page Module For Yee CMS
 *
 * @author Zengkai <zengkai001@qq.com>
 */
class CarouselModule extends \yii\base\Module
{
    /**
     * Version number of the module.
     */
    const VERSION = '0.1.0';

    public $controllerNamespace = 'yeesoft\carousel\controllers';

}