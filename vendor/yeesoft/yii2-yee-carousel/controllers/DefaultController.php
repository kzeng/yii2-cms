<?php

namespace yeesoft\carousel\controllers;

use Yii;
use yeesoft\controllers\admin\BaseController;

use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yeesoft\carousel\models\Carousel;
/**
 * Controller implements the CRUD actions for Carousel model.
 */
class DefaultController extends BaseController
{
    public $modelClass = 'yeesoft\carousel\models\Carousel';
    public $modelSearchClass = 'yeesoft\carousel\models\search\CarouselSearch';

    protected function getRedirectPage($action, $model = null)
    {
        switch ($action) {
            case 'update':
                return ['update', 'id' => $model->id];
                break;
            case 'create':
                return ['update', 'id' => $model->id];
                break;
            default:
                return parent::getRedirectPage($action, $model);
        }
    }

    
    public function actionImageupload()
    {
    	$model = new Carousel;
    	
    	$imageFile = UploadedFile::getInstance($model, 'image');
    	
    	$directory = Yii::getAlias('@frontend/web/uploads/carousel') . DIRECTORY_SEPARATOR;
    	if (!is_dir($directory)) {
    		FileHelper::createDirectory($directory);
    	}
    	
    	if ($imageFile) {
    		$uid = uniqid(time(), true);
    		$fileName = $uid . '.' . $imageFile->extension;
    		$filePath = $directory . $fileName;
    		if ($imageFile->saveAs($filePath)) {
    			$path = 'http://cms.mitoto.cn/uploads/carousel/'  . $fileName;
    			
    			$model->img_url = $path;
    			return Json::encode([
    					'files' => [
    							[
    									'name' => $fileName,
    									'size' => $imageFile->size,
    									'url' => $path,
    									'thumbnailUrl' => $path,
    									//'deleteUrl' => 'image-delete?name=' . $fileName,
    									'deleteType' => 'POST',
    							],
    					],
    			]);
    			
    			
    		}
    	}
    	
    	return '';
    }
}