<?php

use yii\db\Migration;

class m151122_145301_i18n_zh_menu_user extends Migration
{

    public function up()
    {
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'user', 'label' => '用户', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'user-groups', 'label' => '用户分组', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'user-log', 'label' => '用户日志', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'user-permission', 'label' => '用户权限', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'user-role', 'label' => '用户角色', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'user-user', 'label' => '用户', 'language' => 'zh-CN']);
    }

}
