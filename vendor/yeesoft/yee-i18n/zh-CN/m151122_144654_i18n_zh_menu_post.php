<?php

use yii\db\Migration;

class m151122_144654_i18n_zh_menu_post extends Migration
{

    public function up()
    {
        
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'post', 'label' => '文章', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'post-post', 'label' => '文章', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'post-category', 'label' => '分类', 'language' => 'zh-CN']);

    }

}