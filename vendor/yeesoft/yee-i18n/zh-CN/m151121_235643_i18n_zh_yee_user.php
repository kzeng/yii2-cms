<?php

use yeesoft\db\TranslatedMessagesMigration;

class m151121_235643_i18n_zh_yee_user extends TranslatedMessagesMigration
{

    public function getLanguage()
    {
        return 'zh-CN';
    }

    public function getCategory()
    {
        return 'yee/user';
    }

    public function getTranslations()
    {
        return [
            'Child permissions' => '子权限',
            'Child roles' => '子角色',
            'Create Permission Group' => '创建权限组',
            'Create Permission' => '创建权限',
            'Create Role' => '创建角色',
            'Create User' => '创建用户',
            'Log {id}' => 'Log {id}',
            'No users found.' => '用户未找到.',
            'Password' => '密码',
            'Permission Groups' => '权限组',
            'Permission' => '权限',
            'Permissions for "{role}" role' => '角色"{role}"的权限',
            'Permissions' => '权限',
            'Refresh routes' => 'Оновити шляхи',
            'Registration date' => 'Дата реєстрації',
            'Role' => '角色',
            'Roles and Permissions for "{user}"' => 'Ролі і Дозволи для "{user}"',
            'Roles' => '角色',
            'Routes' => 'Маршрути',
            'Search route' => 'Пошук маршруту',
            'Show all' => 'Показати всі',
            'Show only selected' => 'Показати лише вибрані',
            'Update Permission Group' => 'Оновити Групу Дозволів',
            'Update Permission' => 'Оновити Право',
            'Update Role' => 'Оновити Роль',
            'Update User Password' => 'Оновити Пароль Користувача',
            'Update User' => 'Оновити Користувача',
            'User not found' => 'Користувач не знайдений',
            'User' => '用户',
            'Users' => '用户',
            'Visit Log' => '查看日志',
            'You can not change own permissions' => 'Ви не можете змінити власні параметри доступу',
            "You can't update own permissions!" => "Ви не можете оновлювати свої права доступу!",
            '{permission} Permission Settings' => 'Налаштування Дозволу "{permission}"',
            '{permission} Role Settings' => 'Налаштування Ролі "{permission}"',
        ];
    }
}