<?php

use yeesoft\db\TranslatedMessagesMigration;

class m151121_233813_i18n_zh_yee_post extends TranslatedMessagesMigration
{

    public function getLanguage()
    {
        return 'zh-CN';
    }

    public function getCategory()
    {
        return 'yee/post';
    }

    public function getTranslations()
    {
        return [
            'No posts found. ' => '未找到文章.',
            'Post' => '文章',
            'Posted in' => '文章',
            'Posts Activity' => '文章活动',
            'Posts' => '文章',
            'Thumbnail' => '缩略图',
        ];
    }
}