<?php

use yeesoft\db\TranslatedMessagesMigration;

class m151121_225904_i18n_zh_yee_media extends TranslatedMessagesMigration
{

    public function getLanguage()
    {
        return 'zh-CN';
    }

    public function getCategory()
    {
        return 'yee/media';
    }

    public function getTranslations()
    {
        return [
            'Add files' => '增加文件',
            'Album' => '像册',
            'Albums' => '像册',
            'All Media Items' => '所有条目',
            'Alt Text' => '提示',
            'Back to file manager' => '返回文件管理器',
            'Cancel upload' => '取消上传',
            'Categories' => '分类',
            'Category' => '分类',
            'Changes have been saved.' => '修改已保存.',
            'Changes haven\'t been saved.' => '修改未被保存.',
            'Create Category' => '创建分类',
            'Current thumbnail sizes' => '当前缩略图大小',
            'Dimensions' => '维度',
            'Do resize thumbnails' => '重做缩略图',
            'File Size' => '文件大小',
            'Filename' => '文件名',
            'If you change the thumbnails sizes, it is strongly recommended resize image thumbnails.' => '改变size后，推荐重新生成缩略图',
            'Image Settings' => '图片设置',
            'Large size' => '大号',
            'Manage Albums' => '管理像册',
            'Manage Categories' => '管理分类',
            'Media Activity' => '多媒体活动',
            'Media Details' => '多媒体详情',
            'Media' => '多媒体',
            'Medium size' => '中号',
            'No images found.' => '未找到图片.',
            'Original' => '原始',
            'Please, select file to view details.' => '选择文件查看详情.',
            'Select image size' => '选择图片大小',
            'Small size' => '小号',
            'Start upload' => '开始上传',
            'Thumbnails settings' => '缩略图设置',
            'Thumbnails sizes has been resized successfully!' => '缩略图重新生成成功',
            'Thumbnails' => '缩略图',
            'Update Category' => '修改分类',
            'Updated By' => '修改',
            'Upload New File' => '上传新文件',
            'Uploaded By' => '上传',
        ];
    }
}