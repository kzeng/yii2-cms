<?php

use yii\db\Migration;

class m151122_143310_i18n_zh_menu_core extends Migration
{

    public function up()
    {
        $this->insert('{{%menu_lang}}', ['menu_id' => 'admin-menu', 'language' => 'zh-CN', 'title' => '管理菜单']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'dashboard', 'label' => '仪表盘', 'language' => 'zh-CN']);
    }

}