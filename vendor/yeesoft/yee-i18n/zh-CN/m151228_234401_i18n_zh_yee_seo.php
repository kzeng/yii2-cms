<?php

use yeesoft\db\TranslatedMessagesMigration;

class m151228_234401_i18n_zh_yee_seo extends TranslatedMessagesMigration
{

    public function getLanguage()
    {
        return 'zh-CN';
    }

    public function getCategory()
    {
        return 'yee/seo';
    }

    public function getTranslations()
    {
        return [
            'Create SEO Record' => '新建SEO',
            'Follow' => '跟随',
            'Index' => '索引',
            'Keywords' => '关键词',
            'SEO' => 'SEO',
            'Search Engine Optimization' => '搜索引擎优化',
            'Update SEO Record' => '更新SEO记录',
        ];
    }
}