<?php

use yeesoft\db\TranslatedMessagesMigration;

class m151121_210059_i18n_zh_yee_translation extends TranslatedMessagesMigration
{

    public function getLanguage()
    {
        return 'zh-CN';
    }

    public function getCategory()
    {
        return 'yee/translation';
    }

    public function getTranslations()
    {
        return [
            'Add New Source Message' => '增加新的源消息',
            'Category' => '分类',
            'Create Message Source' => '创建消息来源',
            'Create New Category' => '创建新的分类',
            'Immutable' => '不变的',
            'Message Translation' => '消息翻译',
            'New Category Name' => '新分类名称',
            'Please, select message group and language to view translations...' => '选择消息组和语音查看翻译...',
            'Source Message' => '来源消息',
            'Update Message Source' => '信息消息来源',
            '{n, plural, =1{1 message} other{# messages}}' => '{n, plural, =1{1 消息} other{# 消息}}',
        ];
    }

}