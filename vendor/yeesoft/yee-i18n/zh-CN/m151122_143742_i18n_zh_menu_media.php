<?php

use yii\db\Migration;

class m151122_143742_i18n_zh_menu_media extends Migration
{

    public function up()
    {
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'media', 'label' => '多媒体', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'media-media', 'label' => '多媒体', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'media-album', 'label' => '像册', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'media-category', 'label' => '多媒体分类', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'image-settings', 'label' => '图片设置', 'language' => 'zh-CN']);

    }

}