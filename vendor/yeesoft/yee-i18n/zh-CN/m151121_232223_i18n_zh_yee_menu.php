<?php

use yeesoft\db\TranslatedMessagesMigration;

class m151121_232223_i18n_zh_yee_menu extends TranslatedMessagesMigration
{

    public function getLanguage()
    {
        return 'zh-CN';
    }

    public function getCategory()
    {
        return 'yee/menu';
    }

    public function getTranslations()
    {
        return [
            'Menu' => '菜单',
            'Menus' => '菜单',
            'Link ID can only contain lowercase alphanumeric characters, underscores and dashes.' => 'ID 只能包含字母.',
            'Parent Link' => '父链接',
            'Always Visible' => '总是可见',
            'No Parent' => '无父结点',
            'Create Menu Link' => '菜单链接',
            'Update Menu Link' => '菜单链接',
            'Menu Links' => '菜单链接',
            'Add New Menu' => '新增菜单链接',
            'Add New Link' => '新增链接',
            'An error occurred during saving menu!' => '保存菜单出错!',
            'The changes have been saved.' => '修改已保存成功.',
            'Please, select menu to view menu links...' => '请选择菜单查看...',
            'Selected menu doesn\'t contain any link. Click "Add New Link" to create a link for this menu.' => '选中的菜单不包含任何链接.',
        ];
    }
}