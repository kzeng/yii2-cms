<?php

use yeesoft\db\TranslatedMessagesMigration;

class m151121_233713_i18n_zh_yee_page extends TranslatedMessagesMigration
{

    public function getLanguage()
    {
        return 'zh-CN';
    }

    public function getCategory()
    {
        return 'yee/page';
    }

    public function getTranslations()
    {
        return [
            'Page' => '页面',
            'Pages' => '页面',
            'Create Page' => '创建页面',
        ];
    }
}