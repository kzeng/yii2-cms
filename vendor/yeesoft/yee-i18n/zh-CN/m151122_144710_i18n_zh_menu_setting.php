<?php

use yii\db\Migration;

class m151122_144710_i18n_zh_menu_setting extends Migration
{

    public function up()
    {
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'settings', 'label' => '设置', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'settings-general', 'label' => '通用设置', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'settings-reading', 'label' => '读取设置', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'settings-cache', 'label' => '缓存设置', 'language' => 'zh-CN']);
    }

}