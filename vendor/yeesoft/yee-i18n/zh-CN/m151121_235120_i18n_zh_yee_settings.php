<?php

use yeesoft\db\TranslatedMessagesMigration;

class m151121_235120_i18n_zh_yee_settings extends TranslatedMessagesMigration
{

    public function getLanguage()
    {
        return 'zh-CN';
    }

    public function getCategory()
    {
        return 'yee/settings';
    }

    public function getTranslations()
    {
        return [
            'General Settings' => '一般设置',
            'Reading Settings' => '读取设置',
            'Site Title' => '网站标题',
            'Site Description' => '网站描述',
            'Admin Email' => '管理员邮箱',
            'Timezone' => '时间',
            'Date Format' => '日期格式',
            'Time Format' => '时间格式',
            'Page Size' => '每页条数',
        ];
    }
}