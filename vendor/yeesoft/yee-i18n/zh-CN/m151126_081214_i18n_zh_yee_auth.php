<?php

use yeesoft\db\TranslatedMessagesMigration;

class m151126_081214_i18n_zh_yee_auth extends TranslatedMessagesMigration
{

    public function getLanguage()
    {
        return 'zh-CN';
    }

    public function getCategory()
    {
        return 'yee/auth';
    }

    public function getTranslations()
    {
        return [
            'Are you sure you want to delete your profile picture?' => 'Ви дійсно хочете видалити зображення профілю?',
            'Are you sure you want to unlink this authorization?' => 'Ви дійсно хочете видалити цю авторизацію?',
            'Authentication error occurred.' => '认证错误.',
            'Authorization' => '认证',
            'Authorized Services' => '认证服务',
            'Captcha' => '校验码',
            'Change profile picture' => '修改图像',
            'Check your E-mail for further instructions' => 'Перевірте вашу електронну пошту для отримання подальших інструкцій',
            'Check your e-mail {email} for instructions to activate account' => 'Перевірте вашу електронну пошту {email} для подальших інструкцій щодо активації облікового запису',
            'Click to connect with service' => 'Натисніть, щоб авторизуватись з сервісом',
            'Click to unlink service' => 'Натисніть, щоб від\'єднатись від сервісу',
            'Confirm E-mail' => 'Підтвердіть електронну пошту',
            'Confirm' => '确认',
            'Could not send confirmation email' => 'Не вдалося відправити підтвердження по електронній пошті',
            'Current password' => 'Поточний пароль',
            'E-mail confirmation for' => 'Підтвердження адреси електронної пошти для',
            'E-mail confirmed' => 'Електронна пошта підтверджена',
            'E-mail is invalid' => 'Недійсна електронна пошта',
            'E-mail with activation link has been sent to <b>{email}</b>. This link will expire in {minutes} min.' => 'Лист, з посиланням для активації, був відправлений на e-mail <b>{email}</b>. Це посилання буде активним протягом {minutes} хв.',
            'E-mail' => '邮箱',
            'Forgot password?' => '忘记密码?',
            'Incorrect username or password' => 'Неправильне ім\'я користувача або пароль',
            'Login has been taken' => 'Логін зайнятий',
            'Login' => '登录',
            'Logout' => '退出',
            'Non Authorized Services' => 'Неавторизовані Сервіси',
            'Password has been updated' => 'Пароль успішно оновлений',
            'Password recovery' => 'Відновлення пароля',
            'Password reset for' => 'Відновлення пароля для',
            'Password' => '密码',
            'Registration - confirm your e-mail' => 'Реєстрація - Підтвердіть адресу електронної пошти',
            'Registration' => 'Реєстрація',
            'Remember me' => '记住我',
            'Remove profile picture' => 'Видалити зображення профілю',
            'Repeat password' => '重复密码',
            'Reset Password' => '重置密码',
            'Reset' => '复位',
            'Save Profile' => 'Зберегти Профіль',
            'Save profile picture' => 'Зберегти зображення профілю',
            'Set Password' => 'Встановити Пароль',
            'Set Username' => 'Встановити Ім\'я Користувача',
            'Signup' => 'Реєстрація',
            'This E-mail already exists' => 'Цей e-mail вже існує',
            'Token not found. It may be expired' => 'Посилання не дійсне. Воно може бути застарлим',
            'Token not found. It may be expired. Try reset password once more' => 'Посилання не дійсне. Воно може бути застарлим. Спробуйте відновити пароль ще раз',
            'Too many attempts' => 'Занадто багато спроб',
            'Unable to send message for email provided' => 'Не вдалося відправити повідомлення на електронну пошту',
            'Update Password' => 'Змінити пароль',
            'User Profile' => '账号资料',
            "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it." => 'Користувач з такою ж електронною поштою як і в акаунті {client} вже існує. Ввійдіть за допомогою електронної пошти, та прив\'яжіть ваш акаунт.',
            'The username should contain only Latin letters, numbers and the following characters: "-" and "_".' => 'Логін може містити лише латинські літери, цифри та символи "-" і "_".',
            'Username contains not allowed characters or words.' => 'Ім\'я користувача містить недозволені символи або слова.',
            'Wrong password' => 'Неправильний пароль',
            'You could not login from this IP' => 'Ви не можете увійти з цієї IP адреси',
        ];
    }
}