<?php

use yii\db\Migration;

class m151122_122314_i18n_zh_init_demo extends Migration
{
    public function up()
    {
        $this->insert('{{%menu_lang}}', ['menu_id' => 'main-menu', 'language' => 'zh-CN', 'title' => '主菜单']);

        $this->insert('{{%menu_link_lang}}', ['link_id' => 'home', 'label' => '首页', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'about', 'label' => '关于', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'test-page', 'label' => '测试页', 'language' => 'zh-CN']);
        $this->insert('{{%menu_link_lang}}', ['link_id' => 'contact', 'label' => '联系我们', 'language' => 'zh-CN']);

        $this->insert('{{%post_tag_lang}}', ['post_tag_id' => '1', 'title' => 'YeeCMS', 'language' => 'zh-CN']);
        $this->insert('{{%post_tag_lang}}', ['post_tag_id' => '2', 'title' => 'Yii2', 'language' => 'zh-CN']);
        $this->insert('{{%post_category_lang}}', ['post_category_id' => '1', 'title' => '第一个分类', 'language' => 'zh-CN']);

        $this->insert('{{%post_lang}}', ['post_id' => '1', 'title' => '首个文章', 'language' => 'zh-CN',
            'content' => '<p style="text-align: justify;">首个文章.</p>' .
                '<p style="text-align: justify;">首个文章.</p>']);

        $this->insert('{{%post_lang}}', ['post_id' => '2', 'title' => '第二个文章', 'language' => 'zh-CN',
            'content' => '<p style="text-align: justify;">第二个文章.</p>' .
                '<p style="text-align: justify;">第二个文章</p>']);

    }

}
