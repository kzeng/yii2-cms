<?php

$vendorDir = dirname(__DIR__);

return array (
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '2.17.1.0',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'omgdef/yii2-multilingual-behavior' => 
  array (
    'name' => 'omgdef/yii2-multilingual-behavior',
    'version' => '2.1.2.0',
    'alias' => 
    array (
      '@omgdef/multilingual' => $vendorDir . '/omgdef/yii2-multilingual-behavior/src',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'yiisoft/yii2-httpclient' => 
  array (
    'name' => 'yiisoft/yii2-httpclient',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/httpclient' => $vendorDir . '/yiisoft/yii2-httpclient',
    ),
  ),
  'yiisoft/yii2-authclient' => 
  array (
    'name' => 'yiisoft/yii2-authclient',
    'version' => '2.1.2.0',
    'alias' => 
    array (
      '@yii/authclient' => $vendorDir . '/yiisoft/yii2-authclient',
    ),
  ),
  '2amigos/yii2-gallery-widget' => 
  array (
    'name' => '2amigos/yii2-gallery-widget',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@dosamigos/gallery' => $vendorDir . '/2amigos/yii2-gallery-widget/src',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.1.0.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  'yiidoc/yii2-timeago' => 
  array (
    'name' => 'yiidoc/yii2-timeago',
    'version' => '2.0.0.0',
    'alias' => 
    array (
      '@yii/timeago' => $vendorDir . '/yiidoc/yii2-timeago',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.9.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'paulzi/yii2-nested-intervals' => 
  array (
    'name' => 'paulzi/yii2-nested-intervals',
    'version' => '1.0.7.0',
    'alias' => 
    array (
      '@paulzi/nestedintervals' => $vendorDir . '/paulzi/yii2-nested-intervals',
    ),
  ),
  'yeesoft/yii2-yee-generator' => 
  array (
    'name' => 'yeesoft/yii2-yee-generator',
    'version' => '0.1.0.0',
    'alias' => 
    array (
      '@yeesoft/generator' => $vendorDir . '/yeesoft/yii2-yee-generator',
    ),
  ),
  'yeesoft/yii2-multilingual' => 
  array (
    'name' => 'yeesoft/yii2-multilingual',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@yeesoft/multilingual' => $vendorDir . '/yeesoft/yii2-multilingual/src',
    ),
  ),
  '2amigos/yii2-file-upload-widget' => 
  array (
    'name' => '2amigos/yii2-file-upload-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@dosamigos/fileupload' => $vendorDir . '/2amigos/yii2-file-upload-widget/src',
    ),
  ),
);
