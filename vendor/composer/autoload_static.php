<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit21d92ddd9aa6e3646672cf74aca4eece
{
    public static $files = array (
        '2cffec82183ee1cea088009cef9a6fc3' => __DIR__ . '/..' . '/ezyang/htmlpurifier/library/HTMLPurifier.composer.php',
        '0e6d7bf4a5811bfa5cf40c5ccd6fae6a' => __DIR__ . '/..' . '/symfony/polyfill-mbstring/bootstrap.php',
        'a0edc8309cc5e1d60e3047b5df6b7052' => __DIR__ . '/..' . '/guzzlehttp/psr7/src/functions_include.php',
        'c964ee0ededf28c96ebd9db5099ef910' => __DIR__ . '/..' . '/guzzlehttp/promises/src/functions_include.php',
        '2c102faa651ef8ea5874edb585946bce' => __DIR__ . '/..' . '/swiftmailer/swiftmailer/lib/swift_required.php',
        '37a3dc5111fe8f707ab4c132ef1dbc62' => __DIR__ . '/..' . '/guzzlehttp/guzzle/src/functions_include.php',
        '9e090711773bfc38738f5dbaee5a7f14' => __DIR__ . '/..' . '/overtrue/wechat/src/Payment/helpers.php',
    );

    public static $prefixLengthsPsr4 = array (
        'y' => 
        array (
            'yii\\timeago\\' => 12,
            'yii\\swiftmailer\\' => 16,
            'yii\\jui\\' => 8,
            'yii\\imagine\\' => 12,
            'yii\\httpclient\\' => 15,
            'yii\\gii\\' => 8,
            'yii\\faker\\' => 10,
            'yii\\debug\\' => 10,
            'yii\\composer\\' => 13,
            'yii\\codeception\\' => 16,
            'yii\\bootstrap\\' => 14,
            'yii\\authclient\\' => 15,
            'yii\\' => 4,
            'yeesoft\\yee-i18n\\' => 17,
            'yeesoft\\user\\' => 13,
            'yeesoft\\translation\\' => 20,
            'yeesoft\\settings\\' => 17,
            'yeesoft\\seo\\' => 12,
            'yeesoft\\post\\' => 13,
            'yeesoft\\page\\' => 13,
            'yeesoft\\multilingual\\' => 21,
            'yeesoft\\menu\\' => 13,
            'yeesoft\\media\\' => 14,
            'yeesoft\\generator\\' => 18,
            'yeesoft\\eav\\' => 12,
            'yeesoft\\comments\\' => 17,
            'yeesoft\\comment\\' => 16,
            'yeesoft\\block\\' => 14,
            'yeesoft\\carousel\\' => 17,
            'yeesoft\\auth\\' => 13,
            'yeesoft\\' => 8,
        ),
        'w' => 
        array (
            'webvimark\\extensions\\DateRangePicker\\' => 37,
        ),
        'r' => 
        array (
            'rmrevin\\yii\\fontawesome\\' => 24,
        ),
        'p' => 
        array (
            'paulzi\\nestedintervals\\' => 23,
        ),
        'o' => 
        array (
            'omgdef\\multilingual\\' => 20,
        ),
        'd' => 
        array (
            'dosamigos\\qrcode\\' => 17,
            'dosamigos\\gallery\\' => 18,
            'dosamigos\\fileupload\\' => 21,
        ),
        'c' => 
        array (
            'cebe\\markdown\\' => 14,
        ),
        'S' => 
        array (
            'Symfony\\Polyfill\\Mbstring\\' => 26,
            'Symfony\\Component\\HttpFoundation\\' => 33,
            'Symfony\\Bridge\\PsrHttpMessage\\' => 30,
        ),
        'P' => 
        array (
            'Psr\\Log\\' => 8,
            'Psr\\Http\\Message\\' => 17,
            'Psr\\Container\\' => 14,
        ),
        'O' => 
        array (
            'Overtrue\\Socialite\\' => 19,
        ),
        'M' => 
        array (
            'Monolog\\' => 8,
        ),
        'G' => 
        array (
            'GuzzleHttp\\Psr7\\' => 16,
            'GuzzleHttp\\Promise\\' => 19,
            'GuzzleHttp\\' => 11,
        ),
        'F' => 
        array (
            'Faker\\' => 6,
        ),
        'E' => 
        array (
            'EasyWeChat\\' => 11,
        ),
        'D' => 
        array (
            'Doctrine\\Common\\Cache\\' => 22,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'yii\\timeago\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiidoc/yii2-timeago',
        ),
        'yii\\swiftmailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-swiftmailer',
        ),
        'yii\\jui\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-jui',
        ),
        'yii\\imagine\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-imagine',
        ),
        'yii\\httpclient\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-httpclient',
        ),
        'yii\\gii\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-gii',
        ),
        'yii\\faker\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-faker',
        ),
        'yii\\debug\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-debug',
        ),
        'yii\\composer\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-composer',
        ),
        'yii\\codeception\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-codeception',
        ),
        'yii\\bootstrap\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-bootstrap',
        ),
        'yii\\authclient\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-authclient',
        ),
        'yii\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2',
        ),
        'yeesoft\\yee-i18n\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yee-i18n',
        ),
        'yeesoft\\user\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-yee-user',
        ),
        'yeesoft\\translation\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-yee-translation',
        ),
        'yeesoft\\settings\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-yee-settings',
        ),
        'yeesoft\\seo\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-yee-seo',
        ),
        'yeesoft\\post\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-yee-post',
        ),
        'yeesoft\\page\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-yee-page',
        ),
        'yeesoft\\multilingual\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-multilingual/src',
        ),
        'yeesoft\\menu\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-yee-menu',
        ),
        'yeesoft\\media\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-yee-media',
        ),
        'yeesoft\\generator\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-yee-generator',
        ),
        'yeesoft\\eav\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-yee-eav',
        ),
        'yeesoft\\comments\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-comments',
        ),
        'yeesoft\\comment\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-yee-comment',
        ),
        'yeesoft\\block\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-yee-block',
        ),
        'yeesoft\\carousel\\' =>
         array (
           0 => __DIR__ . '/..' . '/yeesoft/yii2-yee-carousel',
         ),
        'yeesoft\\auth\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-yee-auth',
        ),
        'yeesoft\\' => 
        array (
            0 => __DIR__ . '/..' . '/yeesoft/yii2-yee-core',
        ),
        'webvimark\\extensions\\DateRangePicker\\' => 
        array (
            0 => __DIR__ . '/..' . '/webvimark/date-range-picker',
        ),
        'rmrevin\\yii\\fontawesome\\' => 
        array (
            0 => __DIR__ . '/..' . '/rmrevin/yii2-fontawesome',
        ),
        'paulzi\\nestedintervals\\' => 
        array (
            0 => __DIR__ . '/..' . '/paulzi/yii2-nested-intervals',
        ),
        'omgdef\\multilingual\\' => 
        array (
            0 => __DIR__ . '/..' . '/omgdef/yii2-multilingual-behavior/src',
        ),
        'dosamigos\\qrcode\\' => 
        array (
            0 => __DIR__ . '/..' . '/2amigos/yii2-qrcode-helper/src',
        ),
        'dosamigos\\gallery\\' => 
        array (
            0 => __DIR__ . '/..' . '/2amigos/yii2-gallery-widget/src',
        ),
        'dosamigos\\fileupload\\' => 
        array (
            0 => __DIR__ . '/..' . '/2amigos/yii2-file-upload-widget/src',
        ),
        'cebe\\markdown\\' => 
        array (
            0 => __DIR__ . '/..' . '/cebe/markdown',
        ),
        'Symfony\\Polyfill\\Mbstring\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-mbstring',
        ),
        'Symfony\\Component\\HttpFoundation\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/http-foundation',
        ),
        'Symfony\\Bridge\\PsrHttpMessage\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/psr-http-message-bridge',
        ),
        'Psr\\Log\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/log/Psr/Log',
        ),
        'Psr\\Http\\Message\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/http-message/src',
        ),
        'Psr\\Container\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/container/src',
        ),
        'Overtrue\\Socialite\\' => 
        array (
            0 => __DIR__ . '/..' . '/overtrue/socialite/src',
        ),
        'Monolog\\' => 
        array (
            0 => __DIR__ . '/..' . '/monolog/monolog/src/Monolog',
        ),
        'GuzzleHttp\\Psr7\\' => 
        array (
            0 => __DIR__ . '/..' . '/guzzlehttp/psr7/src',
        ),
        'GuzzleHttp\\Promise\\' => 
        array (
            0 => __DIR__ . '/..' . '/guzzlehttp/promises/src',
        ),
        'GuzzleHttp\\' => 
        array (
            0 => __DIR__ . '/..' . '/guzzlehttp/guzzle/src',
        ),
        'Faker\\' => 
        array (
            0 => __DIR__ . '/..' . '/fzaninotto/faker/src/Faker',
        ),
        'EasyWeChat\\' => 
        array (
            0 => __DIR__ . '/..' . '/overtrue/wechat/src',
        ),
        'Doctrine\\Common\\Cache\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/cache/lib/Doctrine/Common/Cache',
        ),
    );

    public static $prefixesPsr0 = array (
        'P' => 
        array (
            'Pimple' => 
            array (
                0 => __DIR__ . '/..' . '/pimple/pimple/src',
            ),
        ),
        'I' => 
        array (
            'Imagine' => 
            array (
                0 => __DIR__ . '/..' . '/imagine/imagine/lib',
            ),
            'Ikimea\\Browser\\' => 
            array (
                0 => __DIR__ . '/..' . '/ikimea/browser/lib',
            ),
        ),
        'H' => 
        array (
            'HTMLPurifier' => 
            array (
                0 => __DIR__ . '/..' . '/ezyang/htmlpurifier/library',
            ),
        ),
        'D' => 
        array (
            'Diff' => 
            array (
                0 => __DIR__ . '/..' . '/phpspec/php-diff/lib',
            ),
        ),
    );

    public static $classMap = array (
        'TijsVerkoyen\\Akismet\\Akismet' => __DIR__ . '/..' . '/tijsverkoyen/akismet/Akismet.php',
        'TijsVerkoyen\\Akismet\\Exception' => __DIR__ . '/..' . '/tijsverkoyen/akismet/Exception.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit21d92ddd9aa6e3646672cf74aca4eece::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit21d92ddd9aa6e3646672cf74aca4eece::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInit21d92ddd9aa6e3646672cf74aca4eece::$prefixesPsr0;
            $loader->classMap = ComposerStaticInit21d92ddd9aa6e3646672cf74aca4eece::$classMap;

        }, null, ClassLoader::class);
    }
}
